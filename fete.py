# -*- encoding: utf-8 -*-
from raison import raison
from fuite import fuite
from frappe import frappe
import main

def fete():
	c = input("Tu décides d’aller a leur recontre car tu te demandes si ce sont vraiment des zombies ou des expérimentaliens qui ont fait la fête sur la plage toute la nuit.\n\n\t1. Tu tentes de les raisonner.\n\t2. Tu vas leur casser la gueule.\n\t3. Une fois là, tu te dis que la fuite c’était une bonne idée, en fait.\n")
	if c == '1':
		raison()
	elif c == '2':
		frappe()
	elif c == '3':
		fuite()
	else:
		print(main.stupid)
