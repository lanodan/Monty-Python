import time
import datetime

#sec since 1970 = time.mktime(datetime.datetime.now().timetuple()) * 1000
def since70():
  return time.mktime(datetime.datetime.now().timetuple()) * 1000
  
def time(): #well, int(time.time()) is fine too
	return datetime.datetime.now()

def duree(t1, t2):
	return t2 - t1

