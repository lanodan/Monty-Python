#!/usr/bin/python3
# -*- encoding: utf-8 -*-

from time import time

global stupid
stupid = 'Apprend à lire et taper.'

global vie
vie = int('5')
def printLifeBar():
	print('\033[31m', '♥'*vie, '\033[0m')

def JouerSon(fichier):
	import pyaudio, wave

	Chunk = 1024

	if len(fichier) is None:
		print('Il faut définir "fichier"')
	else:
		wf = wave.open(fichier, 'r')
		p = pyaudio.PyAudio()

		stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
		                channels=wf.getnchannels(),
		                rate=wf.getframerate(),
		                output=True)

		data = wf.readframes(Chunk)

		while data != '':
			stream.write(data)
			data = wf.readframes(Chunk)

		stream.stop_stream()
		stream.close()

		p.terminate()

# Le minuteur est reglé a 5 minutes par défault
# False : Temps écoulé / Réponse fausse
# True : Réponse bonne & Temps respecté
def QuestionMinute(question, reponse, time = '5*60'):
	start = int(time.time())
	entree = input(question)
	end = int(time.time())
	if str(end - start) > time:
		return False
	elif entree == reponse:
		return True
	else:
		return False

# Exécuter si ce fichier est le principal
if __name__ == "__main__":
	from intro import intro
	intro()
else:
	printLifeBar()
